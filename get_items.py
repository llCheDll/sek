import ipdb
import csv
import requests
import time
import re
import urllib.request
from concurrent.futures import ThreadPoolExecutor

def get_info(req):
    if(req.status_code == 200):
        try:
            h1_pattern = "<h1[^>]*>(.*?)<\/h1>"
            
            url = req.url
            text = str(req.text)
            text = text.replace('\t', '')
            text = text.replace('\n', '')
            
            h1 = re.findall(h1_pattern, text)[0]
            
            print(h1, url);
            return {'h1': h1, 'url': url}
        except:
            print('Errrrrrrror')
            return False
    else:
        return False

def getUrlOpen(url):
    user_agent = 'Mozilla/5.0 (Windows NT 6.1; Win64; x64)'
    headers = {'User-Agent': user_agent}
    
    req = urllib.request.Request(url, headers=headers)
    url_open = urllib.request.urlopen(req)
    
    return url_open

path = 'files/items/dump.csv'
filepath = 'files/items/items.csv'
urls = []
headers = {'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
counter = 0

with open(path, mode='rt', encoding='utf-8-sig') as file:
    # lines = csv.reader(file)
    lines = file.read()
    lines = lines.split('\n')

    keys = []
    
    
    for alias in lines:
        full_url = alias.strip(',')
        urls.append(full_url)

# results = ''

# with ThreadPoolExecutor(16) as executor:
#     results = executor.map(getUrlOpen, urls)

for url in urls:
    try:
        print(url)
        req = requests.get(url, headers=headers)
    except Exception as e:
        print(e)
        continue

    info = get_info(req)
    
    if(info):
        keys.append(info)
    else:
        print('404')

    if len(keys) >= 5000:
        with open(filepath + '_' + str(counter), 'w') as file:
            fildnames = ['h1', 'url']
            w = csv.DictWriter(file, fildnames)
            w.writeheader()
            w.writerows(keys)
            counter += 1
            keys = []

with open(filepath + '_' + str(counter), 'w') as file:
    fildnames = ['h1', 'url']
    w = csv.DictWriter(file, fildnames)
    w.writeheader()
    w.writerows(keys)