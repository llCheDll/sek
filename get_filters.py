import ipdb
import json
import requests
import re
import csv
import time
import os

from bs4 import BeautifulSoup, Comment
from datetime import datetime
from pprint import pprint



def clickedHtml(url):
    driver = webdriver.Chrome()
    driver.get(url)
    elem = driver.find_elements_by_class_name("show-more")
    for button in elem:
        button.click()
    
    time.sleep(1)
    html = driver.page_source
    driver.close()
    
    return html

def get_pagination_quantity(soup):
    pages = soup.find('ul', {'class': 'pagin'})
    
    if pages:
        pages = pages.find_all('li')
        return pages[-2].text
    else:
        return 0

def get_filter_blocks(soup):
    # try:
    filter_blocks = soup.find_all('div', {'class': 'catalog-sidebar-item'})
    
    result = {}
    
    for filter_block in filter_blocks:
        tmp = filter_block.find('div', {'class': 'catalog-sidebar-title'})

        if tmp is None:
            tmp = filter_block.find('a', {'class': 'catalog-sidebar-title'})
        name = tmp.get_text().strip()

        if name != 'Категория' and name != 'Популярные запросы':
            checkbox = filter_block.find('div', {'class': 'catalog-sidebar-view'})
            hrefs = checkbox.find_all('a', href=True)
            result[name] = [href['href'] for href in hrefs]

    return result

def get_current_active_filter_block(filter_blocks, name):
    for block in filter_blocks:
        if name in block.keys():
            return filter_blocks.index(block)

def get_items_quantity(soup):
    try:
        items = soup.find('div', {'class': 'exhaust-system'})
        items = items.find_all('div', {'class': 'card'})
        return len(items)
    except:
        return 0
    
def get_category_list(url):
    try:
        headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
        
        req = requests.get(url, headers=headers)
        
        time.sleep(1)
        soup = BeautifulSoup(req.text, 'html.parser')
        soup = soup.find('div', {'id': 'subcats'})
        hrefs = soup.find_all('a', href=True)

        result = []
        
        for href in hrefs:
            href_tmp = href['href']
            if href_tmp not in result:
                result.append(href_tmp)
        
        return result
    except:
        return False
        
# def get_category_list(url):
#     try:
#         headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
#
#         req = requests.get(url, headers=headers)
#
#         time.sleep(1)
#
#         soup = BeautifulSoup(req.text, 'html.parser')
#         soup = soup.find('div', {'id': 'nextSection'})
#         hrefs = soup.find_all('span', {'class': 'sectionColumn'})
#         hrefs = [href.a['href'] for href in hrefs]
#
#         return hrefs
#     except:
#         return False

def get_info(req):
    h1_pattern = "<h1[^>]*>(.*?)<\/h1>"

    text = str(req.text)
    text = text.replace('\t', '')
    text = text.replace('\n', '')
    
    h1 = re.findall(h1_pattern, text)[0]
    
    return [h1, req.url, req]
    
def get_all_filters(one_filter_soup):
    urls_blocks = get_filter_blocks(one_filter_soup)
    
    keys = { 'one_filter_data':[], 'two_filter_data':[] }

    if urls_blocks:
        for key in urls_blocks.keys():
            for of_url in urls_blocks[key]:
                print(of_url)

                req = requests.get(base_url+of_url, headers=headers)
                time.sleep(1)
                soup = BeautifulSoup(req.text, 'html.parser')

                items_quantity = get_items_quantity(soup)

                # print('url: '+of_url+' items: '+str(items_quantity))
                if (items_quantity >= 3):
                    one_filter_data = get_info(req)

                    tmp_base = {}
                    tmp_base['h1'] = one_filter_data[0]
                    tmp_base['url'] = one_filter_data[1]
                    print(tmp_base)
                    keys['one_filter_data'].append(tmp_base)
                    print("one_filters_add:", of_url)

                    two_filter_soup = BeautifulSoup(one_filter_data[2].text, 'html.parser')
                    two_filter_blocks = get_filter_blocks(two_filter_soup)

                    for key_two in dict(two_filter_blocks):
                        if key == key_two:
                            two_filter_blocks.pop(key_two)
                            break
                        two_filter_blocks.pop(key_two)

                    for key_two in two_filter_blocks.keys():
                        for tf_url in two_filter_blocks[key_two]:
                            print(tf_url)

                            try:
                                tf_req = requests.get(base_url + tf_url, headers=headers)
                            except:
                                tf_req = requests.get(base_url + tf_url, headers=headers)

                            time.sleep(1)

                            tf_soup = BeautifulSoup(tf_req.text, 'html.parser')

                            items_quantity_two = get_items_quantity(tf_soup)

                            if ( items_quantity_two >= 3 ):
                                two_filter_data = get_info(tf_req)

                                tmp = {}
                                tmp['h1'] = two_filter_data[0]
                                tmp['url'] = two_filter_data[1]

                                print(tmp)

                                keys['two_filter_data'].append(tmp)
                                print("two_filters_add:", tf_url)
                            else:
                                print("two_filter_empty:", tf_url)
                else:
                    print("one_filter_empty:", of_url)
    else:
        return False
                
                    
    print("return time: ")
    print(datetime.now())
    return keys


print("Time start script:")
print(datetime.now())


if ( __name__ == '__main__' ):
    base_url = 'https://sek.com.ua/vysokovoltnoe-oborudovanie/'
    
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
    
    filepath = 'files/filters'
    
    base_categories = get_category_list(base_url)
    pprint(base_categories)

    for category in base_categories:
        filename = category
        filename = filename.split('://')[1]
        filename = filename.split('/')[1:]
        filename.pop()

        path = filepath + '/' + '/'.join(filename)

        if not os.path.isdir(path):
            tmp = filepath
            for part in filename:
                tmp += '/' + part
                os.mkdir(tmp)
        else:
            list_dir = os.path.os.listdir(path)
            if 'base_one_filter.csv' in list_dir:
                print(path)
                continue

        print('after', path)

        req = requests.get(category, headers=headers)
        sub_cat = get_category_list(category)

        if sub_cat:
            base_categories = base_categories + sub_cat

        time.sleep(1)

        sub_soup = BeautifulSoup(req.text, 'html.parser')


        filename_one = path + '/base_one_filter.csv'
        filename_two = path + '/base_two_filter.csv'

        sub_keys = get_all_filters(sub_soup)

        if sub_keys:
            with open(filename_one, 'w') as file:
                fildnames = ['h1', 'url']
                w = csv.DictWriter(file, fildnames)
                w.writeheader()
                w.writerows(sub_keys['one_filter_data'])

            with open(filename_two, 'w') as file:
                fildnames = ['h1', 'url']
                w = csv.DictWriter(file, fildnames)
                w.writeheader()
                w.writerows(sub_keys['two_filter_data'])


        print("Time end script:")
        print(datetime.now())


